#!/usr/bin/env bash

doas mkdir -p /usr/local/etc/pkg/repos

doas cp /etc/pkg/FreeBSD.conf /usr/local/etc/pkg/repos

# change quarterly to latest in the conf file
# then update the list with
# doas pkg update && doas pkg upgrade
