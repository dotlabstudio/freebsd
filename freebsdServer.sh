echo "script to setup a fresh freebsd system - must be root"

echo "update system first - might need a reboot"
freebsd-update fetch
freebsd-update install

echo "update packages"
pkg update; pkg upgrade -y

pkg install -y doas fish bash git wget curl fastfetch htop vim yt-dlp lynis rkhunter chkrootkit clamav ouch tealdeer aspell hunspell en-aspell en-hunspell

echo " "
echo "update clamav"
freshclam

echo " "
echo "setup doas"
cp doas.conf /usr/local/etc/doas.conf

echo " "
echo "change default shell to fish"