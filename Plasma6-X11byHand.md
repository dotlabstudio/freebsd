# KDE Plasma6 X11 by Hand

At this point, the system has KDE Plasma6 installed, but it won’t come up (the login manager isn’t enabled, for instance) so we need some convenience things from The Before Times to do a little testing. (Installed as “automatic” so they are easy to remove later).

```
pkg install -A xinit xterm
```

As a normal user, create ~/.xinitrc and put this in it:
```
#! /bin/sh
exec /usr/local/bin/startplasma-x11
```

Then make it executable and start X11 (like it’s 1994):
```
chmod 755 .xinitrc
```

```
startx
```

Voila. KDE Plasma6. Note that, in this state, there are no applications besides xterm and KInfoCenter (and a handful of other KDE Plasma internal things). There’s no configuration applied to the system. The window manager does not honor alt-tab or alt-F4. Use ctrl-Q to quit KInfoCenter.

But X11 is soooo passé. Let’s move on to the Future!

source reference:
```
https://euroquis.nl/kde/2024/10/08/freebsd14.html
```