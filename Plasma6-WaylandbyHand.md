# KDE Plasma6 Wayland by Hand

# need River first 

# Any Wayland by Hand
First, let’s install some convenience things that will help in debugging.

```
pkg install -A river foot
```

Like I wrote 3 years ago about river, it just works. You’ll need the example configuration file so that super-shift-E exits the compositor and window manager. super-shift-enter gets you a terminal window.

I started river as a regular user with
```
ck-launch-session river
```

Here is a very short script that can launch KDE Plasma6 with software rendering. The resulting desktop experience is rather slow.

```
#! /bin/sh
export KWIN_COMPOSE=Q
exec /usr/local/bin/ck-launch-session \
     /usr/local/lib/libexec/plasma-dbus-run-session-if-needed \
     /usr/local/bin/startplasma-wayland
```


So the TODO part of this post is: figure out why opening dri/card0 fails when kwin_wayland is not using the software renderer.

source reference:
```
https://euroquis.nl/kde/2024/10/08/freebsd14.html
```

