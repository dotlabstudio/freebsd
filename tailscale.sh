#!/usr/bin/env bash

#installing tailscale
doas pkg install -y tailscale
#
#start and enable tailscale services
doas service tailscaled enable
doas service tailscaled start
#login tailscale
doas tailscale up
