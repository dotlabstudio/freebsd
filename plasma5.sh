#!/usr/bin/env bash


doas pkg install --yes kde5 plasma5-sddm-kcm sddm xorg firefox vlc libreoffice inkscape kdenlive filezilla strawberry-qt6 gimp krita digikam meld minder wesnoth pokerth gnome-chess gnuchess knights blender darktable lagrange haruna kamoso picard obs-studio py39-tenacity py39-pdfarranger xournalpp nextcloudclient tor-browser monero-cli xmrig wireshark handbrake k3b onlyoffice-documentserver virtualbox-ose virtualbox-ose-additions helix kooha 

doas sysrc dbus_enable="YES" && doas service dbus start
doas sysrc sddm_enable="YES" && doas service sddm start

#when logging in, avoid the Plasma (Wayland) … default that's presented by sddm – for Plasma, Wayland is not yet reliable.
#source: https://community.kde.org/FreeBSD/Setup
