# FreeBSD Project

This is my public repo for fresh freebsd systems to

- setup doas

- setup tailscale

- setup plasma


During install add user to these groups:

wheel

video

operator

Good reference is:

```
https://cooltrainer.org/a-freebsd-desktop-howto/
```


# Use a larger font in console

The base system includes a selection of console fonts in /usr/share/vt/fonts.

Try a different, larger font size ...
```
doas vidcontrol -f terminus-b32
```

Here is a nice selection of terminus fonts of different sizes, converted for use in the FreeBSD console.

```
https://github.com/LionyxML/freebsd-terminus/tree/master
```

Download, unpack, and place the fonts in /usr/share/vt/fonts.

Try the different sizes ...
```
doas vidcontrol -f ter-u24
```

Make a selection permanent by adding to /etc/rc.conf ...
```
allscreens_flags="-f ter-u24"
```

source: https://www.dwarmstrong.org/freebsd-after-install/

# Update or patch system first 

as root
```
freebsd-update fetch
freebsd-update install
```

reboot and check patch number with
```
uname -r

or

uname -a 
```

# Update software packages 

```
doas pkg update; doas pkg upgrade -y
```

# Install core or server packages 

use installServerPackages.sh script or

```
pkg install -y doas fish bash git wget curl fastfetch htop vim yt-dlp lynis rkhunter chkrootkit clamav ouch tealdeer
```

and copy doas.conf to /usr/local/etc/doas.conf


```
doas cp doas.conf /usr/local/etc/doas.conf
```

the doas.conf assumes user is part of the wheel group and should contains:

```
permit nopass keepenv :wheel
permit nopass :wheel cmd reboot
```

## Setup video drivers

For intel and amd

```
doas pkg install drm-kmod
```

for radeon 
```
doas sysrc kld_list+=radeonkms
```

for nvidia 

```
doas pkg instal nvidia-driver
```

or for a more complete nvidia driver install:
```
doas pkg install nvidia-driver nvidia-settings nvidia-drm-515-kmod libva-intel-driver libva-utils
```

```
doas sysrc kld_list="i915kms nvidia-modeset nvidia-drm linux linux64"
```


To check video 
```
doas pciconf -lv|grep -B4 VGA
```

## Setup sound

Enable sound support at boot in loader.conf, and load it immediately with kldload snd_driver.

```
echo 'snd_driver_load="YES"' >> /boot/loader.conf
```
Then, cat /dev/sndstat to detect and see your available sound devices.

```
cat /dev/sndstat
```

Enabling the hw.snd.default_auto boolean will automatically assign hw.snd.default_unit to newly-attached devices.

```
/etc/sysctl.conf

# S/PDIF out on my MSI board
hw.snd.default_unit=6

# Don't automatically use new sound devices
hw.snd.default_auto=0
```

For sound configurations:

```
cat /etc/rc.conf 
```

should have the following lines:

```
sound_load=“YES”

snd_hda_load=“YES”
```

Select sound devices defaults 

```
doas sysctl hw.snd.default_unit 

doas sysctl hw.snd.defaulf_unit=4
# for example to switch audio devices
```

Reload the drivers or reboot 

```
doas kldload sound 

doas kldload snd_hda
```


## Setup Desktop

```
doas pkg install desktop-installer
```

uses the latest packages instaed of the quarterly snapshots and uses src and ports too.

The desktop-installer requires mutiple reboots and root access instead of doas or sudo.

or use the manual way - the better way. 

use the plasma.sh script on this repo.

## Tealdeer Setup

```
tldr --update
```

## Atuin Setup

```
atuin import auto
```

# Setup new ssh pair keys for a new machine and copy public key to a server

To generate a new ssh pair keys on on a new machine:
```
ssh-keygen -t ed25519 -a 100
```

To copy public ssh keys to a server
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub $USER@<serverIPorName>
```



