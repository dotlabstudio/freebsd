#!/usr/bin/env bash

echo "script to setup plasma 6 on a fresh FreeBSD system"
echo " "
echo "setup the FreeBSD server base first"

pkg install -y pkg git cmake plasma6-plasma sddm drm-kmod

# Source reference:
# https://euroquis.nl/kde/2024/10/08/freebsd14.html

echo "setup graphics driver"
# for amd gpu
# sysrc kld_list=amdgpu

# for radeon cpu/gpu
sysrc kld_list+=radeonkms

echo "Make sure DBus will run as a system service"

sysrc dbus_enable=yes

echo "reboot"