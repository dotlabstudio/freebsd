#!/usr/bin/env bash

doas pkg install -y automount fusefs-exfat fusefs-ext2 fusefs-gphotofs fusefs-hfsfuse fusefs-jmtpfs fusefs-ntfs

# afterwards issue this command
# doas /etc/rc.d/devd restart
