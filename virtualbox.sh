#!/usr/bin/env bash



#
#For a VirtualBox virtual machine:
#
#preset the machine to VBoxSVGA – not VMSVGA
#start the machine
#
doas pkg install -y virtualbox-ose-additions
#
#enable shared clipboard as bidirectional in settings --> general ---> advanced
#
#To enable and start the required services:
doas sysrc vboxguest_enable="YES"
doas sysrc vboxservice_enable="YES"
#
#To start the services, restart the system.
#
#Where Guest Additions are installed:
#1. prefer VBoxSVGA
#
#2. do not enable 3D acceleration (doing so will invisibly lose the preference for VBoxSVGA)
