#!/usr/bin/env bash

doas pkg install -y qemu-guest-agent 

doas sysrc qemu_guest_agent_enable="YES"
